#!/bin/bash
 
################################################################
##
##   MySQL Database Backup Script 
##   Written By: Praneeth Perera
##   Last Update: Sep 03, 2021
##
################################################################
 
export PATH=/bin:/usr/bin:/usr/local/bin
TODAY=`date +"%Y-%m-%d-%H:%M:%S"`
 
################################################################
################## Update below values  ########################
 
DB_BACKUP_PATH='/Users/ej/Public/repos/tracking-db-mysql-docker/dbbackup'
DATABASE_NAME='ejs'
MYSQL_HOST='206.189.186.19'
MYSQL_PORT='3306'
MYSQL_USER='sa'

#################################################################
 
mkdir -p ${DB_BACKUP_PATH}/${TODAY}
echo "Backup started for database - ${DATABASE_NAME}"

mysqldump --ssl-mode=DISABLED -h ${MYSQL_HOST} \
   -P ${MYSQL_PORT} \
   -u ${MYSQL_USER} \
   --server-public-key-path=../public_key.pem \
   -p \
   ${DATABASE_NAME} > ${DB_BACKUP_PATH}/${TODAY}/ejs-db-backup-${TODAY}.sql
 
if [ $? -eq 0 ]; then
  echo "Database backup successfully completed"
else
  echo "Error found during backup"
  exit 1
fi
