#!/bin/bash
################################################################
##
##   User Management Script 
##   Last Update: Aug 23, 2021
##
################################################################
#    Script Goals 
#      - change 'sa' password 
#      - add public key for 'sa'
#     
#    Syntax 
#    $ ./lock-down-items.sh oldpassword newpassword

if [[ "$1" == "" && "$2" == "" ]]; 
then
    echo "./lock-down-items.sh oldpassword newpassword"
fi

#DATABASE INFOR
DATABASE_NAME='ejs'
MYSQL_HOST='198.199.121.161'
MYSQL_PORT='3306'
MYSQL_USER='sa'
MYSQL_PASSWORD="$1"
NEWSA_PASSWORD="$2"
USER1="read-write1"
USER1_PASSWORD="newpassword"
USER2='read-write2'
USER2_PASSWORD='newpassword'
READUSR='readonly'
READUSR_PASSWORD='justread'

echo "dropping users"
# mysql -h ${MYSQL_HOST} \
#    -P${MYSQL_PORT} \
#    -u${MYSQL_USER} \
#    -p${MYSQL_PASSWORD} \
#    ${DATABASE_NAME} -e "DROP USER '$USER1'@'$MYSQL_HOST';
#                         DROP USER '$USER2'@'$MYSQL_HOST';
#                         DROP USER '$READUSR'@'MYSQL_HOST'";

echo "making users"
mysql -h ${MYSQL_HOST} \
   -P${MYSQL_PORT} \
   -u${MYSQL_USER} \
   -p${MYSQL_PASSWORD} \
   ${DATABASE_NAME} -e "ALTER USER '$MYSQL_USER' IDENTIFIED BY '$NEWSA_PASSWORD';
      FLUSH PRIVILEGES;
      CREATE USER '$USER1'@'$MYSQL_HOST' IDENTIFIED BY '$USER1_PASSWORD';
      GRANT ALL ON database TO '$USER1'@'$MYSQL_HOST';
      FLUSH PRIVILEGES;
      CREATE USER '$USER2'@'$MYSQL_HOST' IDENTIFIED BY '$USER2_PASSWORD';
      GRANT ALL ON database TO '$USER2'@'$MYSQL_HOST';
      FLUSH PRIVILEGES;
      CREATE USER '$READUSR'@$'MYSQL_HOST' IDENTIFIED BY '$READUSR_PASSWORD';
      GRANT SELECT, SHOW VIEW ON database TO '$READUSR'@'$MYSQL_HOST';
      FLUSH PRIVILEGES;"
