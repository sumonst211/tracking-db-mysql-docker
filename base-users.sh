#!/bin/bash
################################################################
##
##   Base User Management
##   Last Update: Aug 23, 2021
##
##
##   Syntax
##      ./base-users.sh create access user password 
##
##   e.g 
##      ./base-users.sh create read sammy password1
##      ./base-users.sh chgpass sammy password2 
##      ./base-users.sh create readwrite sally password3 
##      ./base-users.sh remove sammy 
##   Example code
##      ./base-users.sh readonly sammy 123 readonly -- read only users
##      ./base-users.sh readwrite sally 123 -- readwrite and write users
##      ./base-users.sh change sammy -- chnage password users
##      ./base-users.sh remove sammy -- remove users
################################################################

DATABASE_NAME='ejs'
MYSQL_HOST='147.182.130.117'
MYSQL_PORT='3306'
MYSQL_USER='sa'
MYSQL_PASSWORD='987'

if [[ $1 == "readonly" ]];
then
   mysql -h ${MYSQL_HOST} \
         -P${MYSQL_PORT} \
         -u${MYSQL_USER} \
         -p${MYSQL_PASSWORD} \
         ${DATABASE_NAME} -e "CREATE USER '$2'@'localhost' IDENTIFIED BY '$3';
         GRANT SELECT, SHOW VIEW ON $DATABASE_NAME.* TO '$2'@'localhost';
         FLUSH PRIVILEGES;"

elif [[ $1 == "readwrite" ]];
then 
   mysql -h ${MYSQL_HOST} \
         -P${MYSQL_PORT} \
         -u${MYSQL_USER} \
         -p${MYSQL_PASSWORD} \
         ${DATABASE_NAME} -e "CREATE USER '$2'@'localhost' IDENTIFIED BY '$3';
         GRANT ALL ON $DATABASE_NAME.* TO '$2'@'localhost';
         FLUSH PRIVILEGES;"

elif [[ $1 == "change" ]];
then 
   mysql -h ${MYSQL_HOST} \
         -P${MYSQL_PORT} \
         -u${MYSQL_USER} \
         -p${MYSQL_PASSWORD} \
         ${DATABASE_NAME} -e "ALTER USER $2 IDENTIFIED BY '$3';
         FLUSH PRIVILEGES;"

elif [[ $1 == "remove" ]];
then
   mysql -h ${MYSQL_HOST} \
         -P${MYSQL_PORT} \
         -u${MYSQL_USER} \
         -p${MYSQL_PASSWORD} \
         ${DATABASE_NAME} -e "DROP USER $2@'localhost';
         FLUSH PRIVILEGES;"

else
   echo "ERROR !!! Check again with your entries!"
fi